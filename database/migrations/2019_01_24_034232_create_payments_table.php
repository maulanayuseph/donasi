<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('bukti_pembayaran');
            $table->integer('id_order');
            $table->string('type');
            $table->integer('total_nominal');
            $table->integer('total_pembayaran');
            $table->date('tgl_pembayaran');
            $table->integer('bank_tujuan');
            $table->string('rekening_pengirim');
            $table->string('nama_pengirim');
            $table->string('bank_pengirim');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
