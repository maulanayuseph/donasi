<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'user_id', 'device_id', 'campaign_id', 'bank_id', 'nominal', 'nama_pemesan', 'email', 'no_hp',
        'catatan', 'alamat', 'kuisioner', 'is_anonim'
    ];

}
