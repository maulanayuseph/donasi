<?php

namespace App\Http\Controllers;

use App\Model\Web;
use App\Model\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\User;
use Illuminate\Support\Facades\Auth;
use App;
use Lang;
use GuzzleHttp\Client;
use Guzzle\Http\Exception\ClientErrorResponseException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\RedirectResponse; 
use Illuminate\Pagination\LengthAwarePaginator;

class WebController extends Controller
{
    
    public function __construct()
    {
        $this->hostapi  = "https://server1.muslimapp.id";
        $this->devapi   = "https://staging.muslimapp.id";
        $this->apitmp   = "http://13.59.66.165:6000";
    }
    
    public function index()
    {
        Session::forget('user');
        Session::forget('lang');
    }

    public function LogoutToken(Request $request,$token) {
        $http   = new Client();

        if (!empty($token)) {
            Session::forget('user');
            Session::forget('lang');
            $data['token'] = $token;
        } else {
            $data['token'] = '';
        }

       return redirect()->back();
    }

    public function log_out(Request $request) {
        $http   = new Client();
        $user   = Session::get('user');
        
        try {
            $response = $http->request('POST', $this->hostapi.'/LogoutJWT', [
                'headers' => [
                    'Authorization' => 'Basic d2ViMTp3ZWIx',
                    'Accept' => 'Application/json'
                ],
                'json' => [
                    'user_id' => $user['user_id'],
                ]
            ]);

            $get_response       = json_decode((string) $response->getBody(), true);

            Session::forget('user');
            Session::forget('lang');
            
            return redirect('https://muslimapp.id/accounts/#/logouttoken/'.$user['token']);
            return redirect('https://store.muslimapp.id/logouttoken/'.$user['token']);
            
        } catch (RequestException $e) {
            $arr1 = Psr7\str($e->getRequest());
            
            if ($e->hasResponse()) {
                $arr2 = Psr7\str($e->getResponse());
                return redirect()->back()->withErrors(['Try Again', 'The Message']);
            }
        }

        return redirect('/');
    }

    public function log_in(Request $request)
    {
        $http     = new Client();

        try {
            $response = $http->request('POST', $this->hostapi.'/LoginJWT', [
                'headers' => [
                    'Authorization' => 'Basic d2ViMTp3ZWIx',
                    'Content-Type' => 'application/json',
                    'Accept' => 'Application/json'
                ],
                'json' => [
                    'email' => $request->input('email'),
                    'password' => $request->input('password'),
                ]
            ]);

            $get_response       = json_decode((string) $response->getBody(), true);
            if($get_response['status'] == 'error'){
                return redirect()->back()->withErrors(['Email atau Password Salah', 'The Message']);
            }
            
            $response_profile = $http->get($this->hostapi.'/restapi/Profile/?id='.$get_response['data'][0]['id'], [
                'headers' => [
                    'Authorization' => 'Basic d2ViMTp3ZWIx',
                    'Content-Type'  => 'application/json'
                ],
            ]);
            
            $get_response_profile   = json_decode((string) $response_profile->getBody(), true);
            
            $user_arr       = array(
                'user_id'           => $get_response_profile['data']['id'],
                'nama'              => $get_response_profile['data']['nama'],
                'email'             => $get_response_profile['data']['email'],
                'token'             => $get_response_profile['data']['jwt_token'],
                'profile_picture'   => $get_response_profile['data']['profile_picture'],
                'alamat'            => $get_response_profile['data']['alamat'],
                'tgl_lahir'         => $get_response_profile['data']['tgl_lahir'],
                'jenis_kelamin'     => $get_response_profile['data']['jenis_kelamin'],
                'no_hp'             => $get_response_profile['data']['no_hp'],
                'password'          => '',
                'konfirmasipassword' => '',
                'campaign_id'       => 1
            );
            
            Session::put('user',$user_arr);

        } catch (RequestException $e) {
            $arr1 = Psr7\str($e->getRequest());
            
            if ($e->hasResponse()) {
                $arr2 = Psr7\str($e->getResponse());
                return redirect()->back()->withErrors(['Email atau Password Salah', 'The Message']);
            }
        }
        
        sleep(3);
        return redirect('/');
    }

    public function regis(Request $request) {
        $http     = new Client();
       
        try {
            $response = $http->request('POST', $this->hostapi.'/restapi/Register', [
                            'headers' => [
                                'Authorization' => 'Basic d2ViMTp3ZWIx',
                                'Content-Type' => 'application/x-www-form-urlencoded',
                                'Accept' => 'Application/json'
                            ],
                            'form_params' => [
                                'nama' => $request->input('name'),
                                'email' => $request->input('email'),
                                'password' => $request->input('password'),
                                'no_hp'     => '00',
                                'alamat'     => 'jl',
                                'tgl_lahir'     => '1999-09-09'
                            ]
                        ]);
            $get_response       = json_decode((string) $response->getBody(), true);
            
            $user_arr       = array(
                'user_id'   => $get_response['iduser'],
                'nama'      => $request->input('name'),
                'email'     => $request->input('email'),
                'alamat'    => $get_response['data']['alamat'],
                'password'  => $request->input('password'),
                'campaign_id' => 1,
            );

            Session::put('user',$user_arr);

        } catch (RequestException $e) {
            $arr1 = Psr7\str($e->getRequest());
            
            if ($e->hasResponse()) {
                $arr2 = Psr7\str($e->getResponse());
                return redirect()->back()->withErrors(['Email Sudah Digunakan sebelumnya', 'The Message']);
            }
        }

        sleep(3);
        return redirect('/');
    }

    public function about()
    {
        $lang     = Session::get('lang');
        
        if ($lang == 'ENG') { 
            $ext    = '&lang=en';
            $lang2  = 'en'; 
        } else { 
            $ext    = ''; 
            $lang2  = 'idn';
        }
        
        App::getLocale();
        App::setLocale($lang2);

        return view('layouts.web.about.app');
    }

    public function indexapp()
    {
        $http     = new Client();
        
        $lang     = Session::get('lang');
        
        if ($lang == 'ENG') { 
            $ext    = '&lang=en';
            $lang2  = 'en'; 
        } else { 
            $ext    = ''; 
            $lang2  = 'idn';
        }
        
        App::getLocale();
        App::setLocale($lang2);

        $response = $http->get($this->hostapi.'/Donasi/Campaign/get_by?featured=TRUE'.$ext, [
            'headers' => [
                'Authorization' => 'Basic d2ViMTp3ZWIx',
                'Content-Type'  => 'application/json'
            ],
        ]);
        
        $get_response   = json_decode((string) $response->getBody(), true);
        
        $totalorangbaik = 0;
        $totalcampaign  = 0;
        foreach ($get_response['data'] as $data) {
            $response_data = $http->get($this->hostapi.'/Donasi/Campaign/get_by?id_campaign='.$data['campaign_id'], [
                'headers' => [
                    'Authorization' => 'Basic d2ViMTp3ZWIx',
                    'Content-Type'  => 'application/json'
                ],
            ]);
            
            $get        = json_decode((string) $response_data->getBody(), true);
            $get_all    = $get['data'][0]['donor'];
            $jumlah     = count($get_all);
            
            $totalcampaign  += count($get['data']);
            $totalorangbaik += $jumlah;

            $donatur[$data['campaign_id']]  = $jumlah;            
        }

        if (!empty($_GET['token'])) {
            $token = $_GET['token'];
            Session::put('token',$token);

            #get user with token
            try {
                $response_token = $http->request('GET', $this->apitmp.'/Login/check', [
                                'headers' => [
                                    'Authorization' => 'Basic d2ViMTp3ZWIx',
                                    'Accept' => 'Application/json'
                                ],
                                'query' => [
                                    'token' => $token,
                                ]
                            ]);
                $get_responsetoken  = json_decode((string) $response_token->getBody(), true);
                $data_userid        = $get_responsetoken['data']['_id'];
                
                $response_profile = $http->get($this->devapi.'/restapi/Profile/?id='.$data_userid, [
                    'headers' => [
                        'Authorization' => 'Basic d2ViMTp3ZWIx',
                        'Content-Type'  => 'application/json'
                    ],
                ]);
                
                $get_response_profile   = json_decode((string) $response_profile->getBody(), true);
                
                $user_arr       = array(
                    'user_id'   => $get_response_profile['data']['id'],
                    'nama'      => $get_response_profile['data']['nama'],
                    'email'     => $get_response_profile['data']['email'],
                    'alamat'    => $get_response_profile['data']['alamat'],
                    'password'  => '',
                    'campaign_id' => 1,
                );
    
                Session::put('user',$user_arr);
    
            } catch (RequestException $e) {
                $arr1 = Psr7\str($e->getRequest());
                
                if ($e->hasResponse()) {
                    $arr2 = Psr7\str($e->getResponse());
                    return redirect('401');
                }
            }

        } else {
            $token = '';
        }

        return view('layouts.web.app')->with('data',$get_response['data'])->with('jumlah_donatur',$donatur)
                                      ->with('totalorangbaik',$totalorangbaik)->with('totalcampaign',$totalcampaign);
    }

    public function get404() {
        return view('layouts.404');
    }

    public function get401() {
        return view('layouts.401');
    }

    public function donasi(Request $request,$campaign_id)
    {
        $http     = new Client();
        
        $lang     = Session::get('lang');
        if ($lang == 'ENG') { 
            $ext    = '&lang=en';
            $lang2  = 'en'; 
        } else { 
            $ext    = ''; 
            $lang2  = 'idn';
        }
        
        App::getLocale();
        App::setLocale($lang2);

        $response = $http->get($this->hostapi.'/Donasi/Campaign/get_by?id_campaign='.$campaign_id.''.$ext, [
            'headers' => [
                'Authorization' => 'Basic d2ViMTp3ZWIx',
                'Content-Type'  => 'application/json'
            ],
        ]);
        
        $get_response   = json_decode((string) $response->getBody(), true);
        
        #get bank
        $respons    = $http->get($this->hostapi.'/restapi/Bank/List_bank', [
            'headers' => [
                'Authorization' => 'Basic d2ViMTp3ZWIx',
                'Content-Type'  => 'application/json'
            ],
        ]);

        $get_bank   = json_decode((string) $respons->getBody(), true);
        
        $get_donor  = $get_response['data'][0]['donor'];
        
        #pagination
        $currentPage        = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection     = collect($get_donor);
        $perPage            = 5;
        $currentPageItems   = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $paginatedItems     = new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        $paginatedItems->setPath($request->url());
        #endpagination

        return view('layouts.web.page.app')->with('getBank', $get_bank['data'])
                                           ->with('donatur',$paginatedItems)
                                           ->with('data',$get_response['data'][0]);
    }

    public function checkout(Request $request)
    {
        $http           = new Client();

        $nama           = $request->input('nama');
        $no_hp          = $request->input('no_hp');
        $email          = $request->input('email');
        $alamat         = $request->input('alamat');
        $nominal        = str_replace(',', '', $request->input('nominal'));
        $bank           = $request->input('bank');
        $catatan        = $request->input('catatan');
        $campaign_id    = $request->input('campaign_id');
        $kuisioner      = $request->input('kuisioner');
        $is_anonim      = $request->input('is_anonim');

        if ($is_anonim != 1) {
            $is_anonim = 0;
        }
        
        if (isset($nama)) {
            $password = '12345';

            $user_arr       = array(
                'user_id'   => 'guest',
                'nama'      => $nama,
                'email'     => $email,
                'password'  => $password,
                'alamat'    => $alamat,
                'campaign_id' => 1,
            );

            Session::put('user',$user_arr);

            $order_arr      = array(
                "user_id"     => 'guest',
                'device_id'   => 'PC',
                'campaign_id' => $campaign_id,
                'bank_id'     => $bank,
                'nominal'     => $nominal,
                'nama_pemesan'=> $nama,
                'email'       => $email,
                'no_hp'       => $no_hp,
                'catatan'     => $catatan,
                'alamat'      => $alamat,
                'kuisioner'   => $kuisioner,
                'is_anonim'   => $is_anonim
            );
            Session::put('order',$order_arr);

            $order_id           = 'guest';
            $campaign_id        = $campaign_id;

        } else {
            #tidak perlu insert user baru ke database
            $ses = Session::get('user');

            $user_id    = $ses['user_id'];
            $nama       = $ses['nama'];
            $email      = $ses['email'];
            $password   = $ses['password'];
            $alamat     = $ses['alamat'];
            
            $user_arr   = array(
                'user_id'   => $user_id,
                'nama'      => $nama,
                'email'     => $email,
                'password'  => $password,
                'alamat'    => $alamat,
                'campaign_id' => $campaign_id
            );

            Session::put('user',$user_arr);

            $order_arr      = array(
                "user_id"     => $user_id,
                'device_id'   => 'PC',
                'campaign_id' => $campaign_id,
                'bank_id'     => $bank,
                'nominal'     => $nominal,
                'nama_pemesan'=> $nama,
                'email'       => $email,
                'no_hp'       => $no_hp,
                'catatan'     => $catatan,
                'alamat'      => $alamat,
                'kuisioner'   => $kuisioner,
                'is_anonim'   => $is_anonim
            );
            Session::put('order',$order_arr);

            $order_id           = 'guest';
            $campaign_id        = $campaign_id;
        }
        
        sleep(5);
        return redirect()->to('/p/konfirmasi/'.$no_hp.'/'.$campaign_id.'/'.$order_id); 
        
    }

    public function confirmation($id,$campaign_id)
    {
        $http     = new Client();
        
        $lang     = Session::get('lang');
        if ($lang == 'ENG') { 
            $ext    = '&lang=en';
            $lang2  = 'en'; 
        } else { 
            $ext    = ''; 
            $lang2  = 'idn';
        }
        
        App::getLocale();
        App::setLocale($lang2);

        $response = $http->get($this->hostapi.'/Donasi/Campaign/get_by?id_campaign='.$campaign_id.''.$ext, [
            'headers' => [
                'Authorization' => 'Basic d2ViMTp3ZWIx',
                'Content-Type'  => 'application/json'
            ],
        ]);
        
        $get_response   = json_decode((string) $response->getBody(), true);
        
        #get bank
        $response    = $http->get($this->hostapi.'/restapi/Bank/List_bank', [
            'headers' => [
                'Authorization' => 'Basic d2ViMTp3ZWIx',
                'Content-Type'  => 'application/json'
            ],
        ]);

        $get_bank   = json_decode((string) $response->getBody(), true);

        #memisahkan berdasarkan type
        foreach ($get_bank['data'] as $bank) {
            $allbank['bank'][$bank['type']]['id'][]   = $bank['id'];
        }
        
        if (!empty(Session::get('user'))) {
            $user       = Session::get('user');
            $get_order  = Session::get('order');
        } else {
            $user      = '';
            $get_order = '';
        }

        return view('layouts.web.konfirmasi.app')->with('getBank', $get_bank['data'])
                                                 ->with('get_order',$get_order)
                                                 ->with('allbank',$allbank)
                                                 ->with('user',$user)
                                                 ->with('data',$get_response['data'][0]);
    }

    public function saveconfirmation(Request $request)
    {
        $http       = new Client();
        $user       = Session::get('user');
        $get_order  = Session::get('order');
        $bank       = $request->input('bank_id');
        $expl       = explode('|',$bank);
        $bank_id    = $expl[0];
        $typebank   = $expl[1];

        if ($user['user_id'] == 'guest') { $user['user_id'] = 0;}
        
        try {
            $response = $http->request('POST', $this->hostapi.'/Donasi/Order', [
                'headers' => [
                    'Authorization' => 'Basic d2ViMTp3ZWIx',
                    'Content-Type' => 'Application/json'
                ],
                'json' => [
                    'user_id'     => $user['user_id'],
                    'device_id'   => 'PC',
                    'campaign_id' => $get_order['campaign_id'],
                    'bank_id'     => $bank_id,
                    'nominal'     => $get_order['nominal'],
                    'nama_pemesan'=> $get_order['nama_pemesan'],
                    'email'       => $get_order['email'],
                    'no_hp'       => $get_order['no_hp'],
                    'catatan'     => $get_order['catatan'],
                    'alamat'      => $get_order['alamat'],
                    'is_anonim'   => $get_order['is_anonim']
                ]
            ]);

            $get_response       = json_decode((string) $response->getBody(), true);

        } catch (RequestException $e) {
            $arr1 = Psr7\str($e->getRequest());
            
            if ($e->hasResponse()) {
                $arr2 = Psr7\str($e->getResponse());
                return redirect('404');
            }
        }

        if ($typebank == 'VA') {
            try {
                $respon = $http->request('POST', $this->hostapi.'/MDTR/VA/Mandiri/charger', [
                    'headers' => [
                        'Authorization' => 'Basic d2ViMTp3ZWIx',
                        'Content-Type'  => 'Application/json'
                    ],
                    'json' => [
                        'order_id'     => $get_response['data']['order_id'],
                        'type'         => 'DONASI',
                    ]
                ]);
    
                $get_response_va       = json_decode((string) $respon->getBody(), true);
                
                Session::put('result_va',$get_response_va['result']);
            } catch (RequestException $e) {
                $arr1 = Psr7\str($e->getRequest());
                
                if ($e->hasResponse()) {
                    $arr2 = Psr7\str($e->getResponse());
                    return redirect('401');
                }
            }
        }

        sleep(5);
        return redirect()->to('/p/det/konfirmasi/'.$get_response['data']['order_id'].'/'.$get_order['campaign_id'].'/'.$get_order['user_id']); 
    }

    public function paypalPayment($OrderID,$type)
    {
        $http       = new Client();
        
        try {
            $response = $http->request('GET', $this->hostapi.'/PaypalSDK', [
                'headers' => [
                    'Authorization' => 'Basic d2ViMTp3ZWIx',
                    'Content-Type'  => 'text/html'
                ],
                'query' => [
                    'id' => $OrderID,
                    'type' => $type
                ]
            ]);
            
            $get_response       = (string)$response->getBody();
                
        } catch (RequestException $e) {
            $arr1 = Psr7\str($e->getRequest());
            
            if ($e->hasResponse()) {
                $arr2 = Psr7\str($e->getResponse());
                return redirect('404');
            }
        }
        
        return $get_response;
    }

    public function detail_confirmation (Request $request,$order_id,$campaign_id,$user_id)
    {
        $http       = new Client();
        $user       = Session::get('user');
        $get_order  = Session::get('order');
       
        $lang     = Session::get('lang');
        if ($lang == 'ENG') { 
            $ext    = '&lang=en';
            $lang2  = 'en'; 
        } else { 
            $ext    = ''; 
            $lang2  = 'idn';
        }
        
        App::getLocale();
        App::setLocale($lang2);

        $response = $http->get($this->hostapi.'/Donasi/Campaign/get_by?id_campaign='.$get_order['campaign_id'].''.$ext, [
            'headers' => [
                'Authorization' => 'Basic d2ViMTp3ZWIx',
                'Content-Type'  => 'application/json'
            ],
        ]);
        
        $get_response   = json_decode((string) $response->getBody(), true);
        
        #get bank
        $respons    = $http->get($this->hostapi.'/restapi/Bank/List_bank?id='.$get_order['bank_id'], [
            'headers' => [
                'Authorization' => 'Basic d2ViMTp3ZWIx',
                'Content-Type'  => 'application/json'
            ],
        ]);

        $get_bank   = json_decode((string) $respons->getBody(), true);

        return view('layouts.web.detailkonfirmasi.app')->with('get_order', $get_order)
                                                       ->with('order_id', $order_id)
                                                       ->with('get_bank',$get_bank['data'][0])
                                                       ->with('data',$get_response['data'][0]);
    }
    
    public function profile(Request $request)
    {
        $http     = new Client();
        
        $session  = Session::get('user'); 

        $lang     = Session::get('lang');
        
        if ($lang == 'ENG') { 
            $ext    = '&lang=en';
            $lang2  = 'en'; 
        } else { 
            $ext    = ''; 
            $lang2  = 'idn';
        }
        
        App::getLocale();
        App::setLocale($lang2);
        
        $response = $http->get($this->hostapi.'/restapi/Profile/?id='.$session['user_id'], [
            'headers' => [
                'Authorization' => 'Basic d2ViMTp3ZWIx',
                'Content-Type'  => 'application/json'
            ],
        ]);
        
        $get_response   = json_decode((string) $response->getBody(), true);
        
        #get order by user id
        $respon = $http->get($this->hostapi.'/Donasi/Order/get_by?user_id='.$session['user_id'], [
            'headers' => [
                'Authorization' => 'Basic d2ViMTp3ZWIx',
                'Content-Type'  => 'application/json'
            ],
        ]);
        
        $get   = json_decode((string) $respon->getBody(), true);
        
        $get_data           = $get['data'];

        #pagination
        $currentPage        = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection     = collect($get_data);
        $perPage            = 5;
        $currentPageItems   = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $paginatedItems     = new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        $paginatedItems->setPath($request->url());
        #endpagination
        
        $totalnom = 0;
        foreach($get['data'] as $data)
        {
            // if ($data['order_status'] == 0) {
                $totalnom += $data['total_nominal'];
            // }
        }

        return view('layouts.web.profile.app')->with('user',$get_response['data'])
                                              ->with('totalnom',$totalnom)
                                              ->with('get_data',$paginatedItems);

    }

    public function updateProfile(Request $request)
    {
        $http     = new Client();
        
        $session  = Session::get('user'); 
        $postData = array();
        $postData['id'] = $session['user_id'];

        if($request->input('nama') !== ''){
            $postData['nama'] = $request->input('nama');
        }
        if($request->input('nama') !== ''){
            $postData['email'] = $request->input('email');
        }
        if($request->input('nama') !== ''){
            $postData['alamat'] = $request->input('alamat');
        }
        if($request->input('nama') !== ''){
            $postData['no_hp'] = $request->input('no_hp');
        }
        if($request->input('nama') !== ''){
            $postData['tgl_lahir'] = $request->input('tgl_lahir');
        }
        if($request->input('nama') !== ''){
            $postData['jenis_kelamin'] = $request->input('jenis_kelamin');
        }
        if($request->input('new_password') !== ''){
            $postData['password'] = $request->input('new_password');
        }

        $response = $http->request('POST', $this->hostapi.'/restapi/Profile/?id='.$session['user_id'], [
            'headers' => [
                'Authorization' => 'Basic d2ViMTp3ZWIx',
                'Content-Type'  => 'application/x-www-form-urlencoded',
                'Accept'        => 'application/json'
            ],
            'form_params' => $postData
        ]);
        
        $get_response       = json_decode((string) $response->getBody(), true);
        echo json_encode(array("status"=>"success", "message"=>"Success updated", "data"=>$get_response));
    }

}