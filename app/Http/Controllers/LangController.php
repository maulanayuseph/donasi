<?php

namespace App\Http\Controllers;

use App\Model\Lang as LanguageModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use DB;
use App;
use Lang;
use GuzzleHttp\Client;
use Guzzle\Http\Exception\ClientErrorResponseException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\RedirectResponse; 
use Illuminate\Pagination\LengthAwarePaginator;

class LangController extends Controller
{
    public function __construct()
    {
        $this->hostapi  = "https://server.muslimapp.id";
        $this->devapi   = "https://staging.muslimapp.id";
    }

    public function index()
    {
        
    }

    public function lang(Request $request)
    {
        $lang = $request->input('param');
        
        Session::put('lang',$lang);
    }

}
