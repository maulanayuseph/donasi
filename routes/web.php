<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WebController@indexapp')->name('indexapp');
Route::get('/404', 'WebController@get404')->name('404');
Route::get('/401', 'WebController@get401')->name('401');

Route::get('/p/donasi/{detail}', 'WebController@donasi')->name('donasi');
Route::get('/p', 'WebController@index')->name('test');
Route::get('/about', 'WebController@about')->name('about');
Route::get('/p/profile', 'WebController@profile')->name('profile');
Route::post('/p/checkout', 'WebController@checkout')->name('checkout');
Route::get('/p/konfirmasi/{id}/{campaign_id}/{no_hp}', 'WebController@confirmation')->name('confirmation');
Route::get('/p/det/konfirmasi/{id}/{campaign_id}/{no_hp}', 'WebController@detail_confirmation')->name('detail_confirmation');
Route::post('p/confirmation', 'WebController@saveconfirmation')->name('saveconfirmation');
Route::post('/p/profile/update', 'WebController@updateProfile')->name('update_profile');
Route::post('p/lang','LangController@lang')->name('lang');
Route::get('logouttoken/{id}','WebController@LogoutToken')->name('LogoutToken');

Route::get('paypal/{orderid}/{type}','WebController@paypalPayment')->name('paypalPayment');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/register','WebController@regis')->name('register');
Route::post('/login','WebController@log_in')->name('login');
Route::post('/p/logout','WebController@log_out')->name('log_out');