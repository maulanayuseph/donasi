<footer class="site-footer">
    <div class="footer-widgets">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="foot-about">
                        <h2><a class="foot-logo" href="{{url('/')}}">
                                <img src="{{ asset('template.web/css/images/foot-logo.png') }}" alt="">
                            </a>
                        </h2>
                        <?php $seslang = Session::get('lang'); ?>
                        <select class="form-control" name="lang" onchange="changelang(this.value)" style="width:85px;height:30px;margin-top:-30px">
                            <option <?=($seslang == 'IDN') ? 'selected': ''?> value="IDN">IDN</option>
                            <option <?=($seslang == 'ENG') ? 'selected': ''?>  value="ENG">ENG</option>
                        </select>

                        <ul class="d-flex flex-wrap align-items-center">
                            <li>
                                <a href="https://www.instagram.com/muslimapp.id"><i class="fa fa-instagram"></i></a>
                            </li>
                            <li>
                                <a href="https://www.facebook.com/muslimapp.id"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="https://www.twitter.com/muslimapp_id"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="https://www.linkedin.com/company/muslimapp/"><i class="fa fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div><!-- .foot-about -->
                </div><!-- .col -->

                <div class="col-12 col-md-6 col-lg-3 mt-5 mt-md-0">
                    <h2>Useful Links</h2>

                    <ul>
                        <li><a href="#">{{__('all.Donasi')}}</a></li>
                        <li><a href="#">{{__('all.Testimoni')}}</a></li>
                        <li><a href="https://news.muslimapp.id" target="_blank">{{__('all.News')}}</a></li>
                    </ul>
                </div><!-- .col -->

                <div class="col-12 col-md-6 col-lg-3 mt-5 mt-md-0">
                    <div class="foot-latest-news">
                        <h2>{{__('all.event')}}</h2>

                        <ul>
                            <li>
                                <h3><a href="https://muslimapp.id/donasi/p/donasi/1">Berdonasi Untuk Masjid Gaza Palestine</a></h3>
                                <div class="posted-date">January 27, 2018</div>
                            </li>

                            <li>
                                <h3><a href="https://muslimapp.id/donasi/p/donasi/2">Aqiqah Untuk 1000 Anak Yatim</a></h3>
                                <div class="posted-date">January 27, 2018</div>
                            </li>

                        </ul>
                    </div><!-- .foot-latest-news -->
                </div><!-- .col -->

                <div class="col-12 col-md-6 col-lg-3 mt-5 mt-md-0">
                    <div class="foot-contact">
                        <h2>Contact</h2>

                        <ul>
                            <li><i class="fa fa-phone"></i>
                                <a href="https://api.whatsapp.com/send?phone=628112333724" >+62 811 2333 724</a>
                            </li>
                            <li><i class="fa fa-envelope"></i>
                                <a href="mailto:info@muslimapp.id">info@muslimapp.id</a>
                            </li>
                            <li><i class="fa fa-map-marker"></i><span>Jl. Kolenang No. 12, 40321 Bandung, ID</span></li>
                        </ul>
                    </div><!-- .foot-contact -->

                    <!-- <div class="subscribe-form">
                    <span>Newsletter dan info</span>
                        <form class="d-flex flex-wrap align-items-center">
                            <input type="email" placeholder="Your email">
                            <input type="submit" value="send">
                        </form>
                    </div> -->

                </div><!-- .col -->
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- .footer-widgets -->

    <div class="footer-bar">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <p>Hak Cipta PT. Digital Muslim Global &copy <?=date("Y")?> </p>
                </div><!-- .col-12 -->
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- .footer-bar -->
</footer><!-- .site-footer -->