<div class="page-area cart-page spad">
    <div class="container">
        <form class="checkout-form" id="checkout-form" method="POST" action="{{route('checkout')}}" onsubmit="return checkvalid(this)">
        {{ csrf_field() }}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
                <div class="col-lg-6">
                    <h4 class="checkout-title">{{__('all.formdonasi')}}</h4>
                    @if($errors->any())
                        <p align="center">{{$errors->first()}}</p>
                    @endif

                    <div class="row">
                        <div class="col-md-12">
                            @if (Session::get('user'))
                            <?php $ses = Session::get('user'); ?>
                                <div class="top-right links" style="text-align:left">
                                    <b>{{ $ses['nama'] }}</b> <br/>
                                    {{ $ses['email'] }} <br/>
                                    <div class="form-check" style="text-align:left;">
                                        <input class="form-check-input" type="checkbox" value="1" id="is_anonim" name="is_anonim" style="margin-top: 8px;margin-left: 0px;">
                                        <span style="font-size:12px;margin-left:15px">{{__('all.hideme')}}</span>
                                    </div>
                                    <br/>
                                    <input type="text" name="no_hp" class="form-control" placeholder="Number Whatsapp / Handphone *" required>
                                    <input type="hidden" name="email" value="{{$ses['email']}}" class="form-control" placeholder="Email *" required>
                                    <div class="form-group">
                                        <input type="text" name="nominal" aria-describedby="p-error-nominal"  onkeyup="nom()" class="form-control nominal" placeholder="{{__('all.placejumlahdonasi')}}" required style="background-color:#ebebeb;margin-bottom:0px;">
                                        <small class="form-text text-muted" id="p-error-nominal" style="text-align:left;color:darkred !important"></small>
                                    </div>

                                    <textarea class="form-control" name="catatan" rows="2" placeholder="{{__('all.placecatatan')}}" style="background-color:#ebebeb"></textarea>
                                    <textarea class="form-control" name="alamat" placeholder="{{__('all.placeaddress')}}" style="background-color:#ebebeb;display:none" required>{{$ses['alamat']}}</textarea>
                                </div> <br/>
                            @else
                                <div class="top-right links" style="text-align:center">
                                    <a href="{{ route('login') }}">{{__('all.signin')}}</a> {{__('all.lengkapidata')}}
                                    <input type="text" name="nama" class="form-control" placeholder="{{__('all.placenamalengkap')}}" required>
                                    <div class="form-check" style="text-align:left;margin-top:-25px">
                                        <input class="form-check-input" type="checkbox" value="1" id="is_anonim" name="is_anonim" style="margin-top: 8px;margin-left: 0px;">
                                        <span style="font-size:12px;margin-left:15px">{{__('all.hideme')}}</span>
                                    </div>
                                    <input type="text" name="no_hp" class="form-control" placeholder="Number Whatsapp / Handphone *" required>
                                    <input type="email" name="email"  class="form-control" placeholder="Email *" required>
                                    <textarea class="form-control" name="alamat" placeholder="{{__('all.placeaddress')}}" style="background-color:#ebebeb;" required></textarea> <br/>
                                    <div class="form-group">
                                        <input type="text" onkeyup="nom()" aria-describedby="p-error-nominal" name="nominal" class="form-control nominal" placeholder="{{__('all.placejumlahdonasi')}}" style="background-color:#ebebeb;margin-bottom:0px" required>
                                        <small class="form-text text-muted" id="p-error-nominal" style="text-align:left;color:darkred !important"></small>
                                    </div>
                                    
                                    <textarea class="form-control" name="catatan" rows="2" placeholder="{{__('all.placecatatan')}}" style="background-color:#ebebeb"></textarea>
                                </div> <br/>
                            @endif
                        </div>
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <h6 class="mb-0"> {{__('all.darimanatau')}} </h6>
                                </div>
                                <div class="card-body" style="margin-left:10px">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="kuisioner" id="instagram" value="instagram" checked>
                                        <label class="form-check-label" for="instagram">
                                            Instagram
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="kuisioner" id="facebook" value="facebook">
                                        <label class="form-check-label" for="facebook">
                                            Facebook
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="kuisioner" id="twitter" value="twitter">
                                        <label class="form-check-label" for="twitter">
                                            Twitter
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <button type="submit" id="submit" class="btn btn-full btn-primary" style="border:0px;color:#fff">
                                <span id="loading">Continue</span>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="order-card">
                        <div class="order-details">
                            <div class="od-warp">
                                <h4 class="checkout-title"> #{{__('all.nodonasi')}} {{$data['campaign_id']}}</h4>
                                <input type="hidden" value="{{$data['campaign_id']}}" name="campaign_id">
                                <div class="cause-wrap d-flex flex-wrap justify-content-between">
                        <figure class="m-0">
                            <img src="{{$data['campaign_image']}}" alt="">
                        </figure>

                            <div class="cause-content-wrap">
                                <header class="entry-header d-flex flex-wrap align-items-center">
                                    <h3 class="entry-title w-100 m-0">
                                        <a href="#">{{$data['campaign_title']}}</a>
                                    </h3>

                                    <div class="posted-date">
                                        <a href="javascript:;">
                                            <?php $date = date_create($data['campaign_created']); echo date_format($date,"Y/m/d H:i:s");?>
                                        </a>
                                    </div>

                                    <div class="posted-date">
                                        <?php 
                                            $hariini    = new DateTime();
                                            $terakhir   = new DateTime($data['campaign_valid']);
                                            $sisa       = $terakhir->diff($hariini)->format("%a");
                                        ?>
                                        <a href="javascript:;">
                                            {{$sisa}} {{__('all.daysagain')}}
                                        </a>
                                    </div>

                                    <div class="cats-links">
                                        <a href="javascript:;">{{$data['city'][0]['name']}}</a>
                                    </div>

                                </header>

                                <div class="entry-content" style="text-align:justify">
                                    <p class="m-0">{{$data['campaign_description']}}</p>
                                </div>

                                <hr/>

                                <button class="btn btn-primary col-md-12" type="button" style="border:0px;color:#fff" onclick="collapse()">
                                    {{__('all.donor')}} ({{count($data['donor'])}})
                                </button>

                                <div id="collapsedonatur" data-v='0' style="display:none">
                                    <br/>
                                    <div class="list-group" style="font-size:13px">
                                        @foreach ($donatur as $donor)
                                            <a href="javascript:;" class="list-group-item list-group-item-action">
                                                <div class="d-flex w-100 justify-content-between">
                                                <?php $tgl = date_create($donor['order_date']); ?>
                                                <h6 class="mb-1">Rp. {!!number_format($donor['total_nominal'])!!}</h6>
                                                    <small class="text-muted">{!!date_format($tgl,'d M Y H:i:s')!!}</small>
                                                </div>
                                                <p class="mb-1">{!!$donor['nama_pemesan']!!}</p>
                                                <small>{!!$donor['catatan']!!}</small>
                                            </a>
                                        @endforeach
                                        {{ $donatur->links() }}
                                    </div>
                                    
                                </div>


                            </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

@section('js_checkout')
    <script type="text/javascript">

        function collapse() {
            var v = $("#collapsedonatur").attr('data-v');
            
            if (v == 0) {
                $("#collapsedonatur").removeAttr('style');
                $("#collapsedonatur").attr('data-v',1);
            } else {
                $("#collapsedonatur").attr('style','display:none');
                $("#collapsedonatur").attr('data-v',0);
            }
            
            
        }

        function nom()
        {
            var nom     = $(".nominal").val();
            var nominal = nom.replace(",", "");
            
            if ( (nominal%10000)!=0 && nominal>=10000)  {
                $("#p-error-nominal").html('{{__("all.errnominal")}}');
            } else {
                $("#p-error-nominal").html('');
            }
        }

        $("document").ready(function() {
            var cleave = new Cleave('.nominal', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand'
            });
        });

        function checkvalid()
        {
            var err = $("#p-error-nominal").html();
            var form = document.getElementById('checkout-form');
            var isValidForm = form.checkValidity();
            
            if (isValidForm == true && err == '') {
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": true,
                    "positionClass": "toast-top-center",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
                toastr["success"]("Silahkan Tunggu ...");
                $("#submit").attr('disabled','disabled');
                $("#loading").html('Loading ...');
            } else {
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": true,
                    "positionClass": "toast-top-center",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
                toastr["error"]('{{__("all.errnominal")}}');
                $("#submit").removeAttr('disabled');
                $("#loading").html('Continue');
                return false;
            }
        }
    </script>
@endsection
