<div class="home-page-icon-boxes">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-4 mt-4 mt-lg-0">
                <div class="icon-box active">
                    <figure class="d-flex justify-content-center">
                        <img src="{{ asset('template.web/css/images/hands-gray.png') }}" alt="">
                        <img src="{{ asset('template.web/css/images/hands-white.png') }}" alt="">
                    </figure>

                    <header class="entry-header">
                        <h3 class="entry-title">{{__('all.title1')}}</h3>
                    </header>

                    <div class="entry-content">
                        <p>
                            {{__('all.slog1')}}
                            <br/><br/>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-4 mt-4 mt-lg-0">
                <div class="icon-box">
                    <figure class="d-flex justify-content-center">
                        <img src="{{ asset('template.web/css/images/donation-gray.png') }}" alt="">
                        <img src="{{ asset('template.web/css/images/donation-white.png') }}" alt="">
                    </figure>

                    <header class="entry-header">
                        <h3 class="entry-title">{{__('all.title2')}}</h3>
                    </header>

                    <div class="entry-content">
                        <p>
                            {{__('all.slog2')}}
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-4 mt-4 mt-lg-0">
                <div class="icon-box">
                    <figure class="d-flex justify-content-center">
                        <img src="{{ asset('template.web/css/images/charity-gray.png') }}" alt="">
                        <img src="{{ asset('template.web/css/images/charity-white.png') }}" alt="">
                    </figure>

                    <header class="entry-header">
                        <h3 class="entry-title">{{__('all.title3')}}</h3>
                    </header>

                    <div class="entry-content">
                        <p>
                            {{__('all.slog3')}}
                        </p>
                        @if(!empty(Session::get('lang')))
                            @if(Session::get('lang') == 'IDN')
                                <br/>
                            @endif
                        @else
                            <br/>
                        @endif
                    </div>
                </div>
            </div>
        </div><!-- .row -->
    </div><!-- .container -->
</div><!-- .home-page-icon-boxes -->
