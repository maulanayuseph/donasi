<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{__('all.Title')}}</title>
    <link rel="shortcut icon" type="image/png" href="{{ asset('favicon.png') }}"/>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('template.web/css/bootstrap.min.css') }}">

    <!-- FontAwesome CSS -->
    <link rel="stylesheet" href="{{ asset('template.web/css/font-awesome.min.css') }}">

    <!-- ElegantFonts CSS -->
    <link rel="stylesheet" href="{{ asset('template.web/css/elegant-fonts.css') }}">

    <!-- themify-icons CSS -->
    <link rel="stylesheet" href="{{ asset('template.web/css/themify-icons.css') }}">

    <!-- Swiper CSS -->
    <link rel="stylesheet" href="{{ asset('template.web/css/swiper.min.css') }}">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('template.web/css/style.css') }}">
    <link  rel="stylesheet" href="{{ asset('toastr/build/toastr.css') }}"> 
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body style="font-family :open sans,sans-serif">

   <!-- Header -->
    @include('layouts.web.header')
   <!-- //Header -->


    <body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>
	
	<br>

	<!-- Page -->
	@include('layouts.web.profile.content')
	<!-- Page -->
	
    <br/>
      
   <!-- Footer -->
    @include('layouts.web.footer')
   <!-- //Footer -->

    <script type='text/javascript' src='{{ asset('template.web/js/jquery.js') }}'></script>
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js'></script>
    <!-- <script type='text/javascript' src='{{ asset('template.web/js/jquery.min.js') }}'></script> -->
    <script type='text/javascript' src='https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js'></script>
    <!-- <script type='text/javascript' src='{{ asset('template.web/js/bootstrap.min.js') }}'></script> -->
    <script type='text/javascript' src='{{ asset('toastr/toastr.js') }}'></script>
    <script src="{{ asset('cleave.js/dist/cleave.min.js') }}"></script>
    <script src="{{ asset('cleave.js/dist/addons/cleave-phone.id.js') }}"></script>
    <script type='text/javascript' src='{{ asset('template.web/js/jquery.collapsible.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('template.web/js/swiper.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('template.web/js/jquery.countdown.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('template.web/js/circle-progress.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('template.web/js/jquery.countTo.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('template.web/js/jquery.barfiller.js') }}'></script>
    <script type='text/javascript' src='{{ asset('template.web/js/custom.js') }}'></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <!-- <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script> -->

    <script type="text/javascript">
        function changelang(id)
        {
            jQuery.ajax({
                url: "{{route('lang')}}",
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                data: {
                    param : id,
                },
                success : function(data) {
                    location.reload();
                }
            });
        }
    </script>
    
    @yield('js')

</body>
</html>