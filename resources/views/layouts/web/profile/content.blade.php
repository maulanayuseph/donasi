<div class="container">
    <div class="row">
        <div class="col-md-4 col-4 col-sm-4 col-lg-4">
            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
            <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-history" role="tab" aria-controls="v-pills-home" aria-selected="true">{{__('all.mydonasi')}}</a>
            <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Profile</a>
            </div>
        </div>
        <div class="col-md-8 col-8 col-sm-8 col-lg-8">
            <div class="tab-content" id="v-pills-tabContent">

                <div class="tab-pane fade show active" id="v-pills-history" role="tabpanel" aria-labelledby="v-pills-home-tab">
                    <div class="row">
                        <div class="col-md-12" style="text-align:right">
                            <h4>Rp. {{number_format($totalnom)}}</h4>
                            <h6><b>Total {{__('all.Nominal')}}</b></h6>
                        </div>
                    </div>

                    <h6><b>{{__('all.mydonasi')}}</b></h6>
                    <table class="table table-striped table-responsive-lg" style="width:100%;font-size:12px">
                        <thead>
                            <tr class="table-primary">
                            <th scope="col" style="width:35%">Campaign</th>
                            <th scope="col" style="width:25%">{{__('all.nomdonasi')}}</th>
                            <th scope="col" style="width:25%">{{__('all.tanggal')}} Transfer</th>
                            <th scope="col" style="width:10%;text-align:center">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; ?>
                            @foreach($get_data as $data)
                            <tr>
                                <td>
                                    <a href="#" data-toggle="modal" data-target="#detail_modal{{$no}}">
                                        <b>{{$data['order_id']}}</b>
                                    </a>

                                    <!-- The Modal -->
                                    <div class="modal" id="detail_modal{{$no}}">
                                        <div class="modal-dialog" style="">
                                        <div class="modal-content">
                                        
                                            <!-- Modal Header -->
                                            <div class="modal-header">
                                                <h4 class="modal-title">{{$data['order_id']}}</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            
                                            <!-- Modal body -->
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-4">Judul Campaign</div><div class="col-md-1">:</div>
                                                    <div class="col-md-7">{{$data['campaign'][0]['campaign_title']}}</div>
                                                    
                                                    <div class="col-md-4">Deskripsi</div><div class="col-md-1">:</div>
                                                    <div class="col-md-7">
                                                        {!!$data['campaign'][0]['campaign_description']!!}
                                                    </div>

                                                    <div class="col-md-4">Nama Donatur</div><div class="col-md-1">:</div>
                                                    <div class="col-md-7">{{$data['nama_pemesan']}}</div>

                                                    <div class="col-md-4">Nominal</div><div class="col-md-1">:</div>
                                                    <div class="col-md-7">Rp. {{number_format($data['total_nominal'])}}</div>

                                                    <div class="col-md-4">Tanggal Transfer</div><div class="col-md-1">:</div>
                                                    <div class="col-md-7">
                                                        <?php $tgl_order = date_create($data['order_date']); ?>
                                                        {!!date_format($tgl_order,'d M Y H:i:s')!!}
                                                    </div>

                                                    <div class="col-md-4">Status</div><div class="col-md-1">:</div>
                                                    <div class="col-md-7">
                                                        @if ($data['order_status'] == 0)
                                                            <a href="https://payments.muslimapp.id/" target="_blank" alt="konfirmasi pembayaran">
                                                                <div class="alert alert-danger" role="alert" style="text-align:center">
                                                                    {{__('all.blmkonfirmasi')}}
                                                                </div>
                                                            </a>
                                                        @elseif ($data['order_status'] == 1)
                                                            <div class="alert alert-primary" role="alert" style="text-align:center">
                                                                {{__('all.sdhkonfirmasi')}}
                                                            </div>
                                                        @elseif ($data['order_status'] == 2)
                                                            <div class="alert alert-success" role="alert" style="text-align:center">
                                                                {{__('all.sdhbayar')}}
                                                            </div>
                                                        @endif
                                                    </div>

                                                </div>
                                            </div>
                                            
                                            <!-- Modal footer -->
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            </div>
                                            
                                        </div>
                                        </div>
                                    </div>

                                    {!!$data['campaign'][0]['campaign_title']!!}
                                </td>
                                <td>Rp. {{number_format($data['total_nominal'])}}</td>
                                <?php $tgl = date_create($data['order_date']); ?>
                                <td>{!!date_format($tgl,'d M Y H:i:s')!!}</td>
                                <td>
                                    @if ($data['order_status'] == 0)
                                        <a href="https://payments.muslimapp.id/" target="_blank" alt="konfirmasi pembayaran">
                                            <div class="alert alert-danger" role="alert" style="text-align:center">
                                                {{__('all.blmkonfirmasi')}}
                                            </div>
                                        </a>
                                    @elseif ($data['order_status'] == 1)
                                        <div class="alert alert-primary" role="alert" style="text-align:center">
                                            {{__('all.sdhkonfirmasi')}}
                                        </div>
                                    @elseif ($data['order_status'] == 2)
                                        <div class="alert alert-success" role="alert" style="text-align:center">
                                            {{__('all.sdhbayar')}}
                                        </div>
                                    @endif
                                </td>
                            </tr>
                            <?php $no++; ?>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $get_data->links() }}
                </div>
                
                <div class="tab-pane fade show" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab" >
                    <h3>Profile</h3>
                    <form id="form_ubah">
                    <!-- <form id="form_ubah" method="POST" action="{{route('update_profile')}}"> -->
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row" style="padding:5px;">
                            <div class="col-md-3">{{__('all.namalengkap')}} </div><div class="col-md-1">:</div><div id="txt-nama" class="col-md-7">{{$user['nama']}}</div>
                            <div id ="input-nama" style="display: none;" class="col-md-7"> <input type="text" name="nama" class="form-control" placeholder="{{ $user['nama'] }}"></div>
                        </div>
                        <div class="row" style="padding:5px;">
                            <div class="col-md-3">Email</div><div class="col-md-1">:</div><div id="txt-email" class="col-md-7">{{$user['email']}}</div>
                            <div id ="input-email" style="display: none;" class="col-md-7"> <input type="text" name="email" class="form-control" placeholder="{{ $user['email'] }}"></div>
                        </div>
                        <div class="row" style="padding:5px;">
                            <div class="col-md-3">{{__('all.alamat')}}</div><div class="col-md-1">:</div><div id="txt-alamat" class="col-md-7">{{$user['alamat']}}</div>
                            <div id ="input-alamat" style="display: none;" class="col-md-7"> <input type="text" name="alamat" class="form-control" placeholder="{{ $user['alamat'] }}"></div>
                        </div>
                        <div class="row" style="padding:5px;">
                            <div class="col-md-3">No Handphone</div><div class="col-md-1">:</div><div id="txt-no-hp" class="col-md-7">{{$user['no_hp']}}</div>
                            <div id ="input-no-hp" style="display: none;" class="col-md-7"> <input type="text" name="no_hp" class="form-control" placeholder="{{ $user['no_hp'] }}"></div>
                        </div>
                        <div class="row" style="padding:5px;">
                            <div class="col-md-3">{{__('all.tanggallahir')}}</div><div class="col-md-1">:</div><div id="txt-tgl-lhr" class="col-md-7">{{ $user['tgl_lahir']}}</div>
                            <div id ="input-tgl-lhr" style="display: none;" class="col-md-7"> <input type="date" name="tgl_lahir" class="form-control" value="{{ date('Y-m-d', strtotime($user['tgl_lahir'])) }}"></div>
                        </div>
                        <div class="row" style="padding:5px;">
                            <div class="col-md-3">{{__('all.jeniskelamin')}}</div><div class="col-md-1">:</div><div id="txt-jenis-kelamin" class="col-md-7">{{$user['jenis_kelamin']}}</div>
                            <!-- <div id ="input-jenis-kelamin" style="display: none;" class="col-md-7"> <input type="text" name="jenis_kelamin" class="form-control" id=""  placeholder="{{ $user['jenis_kelamin'] }}"></div> -->
                            <div id ="input-jenis-kelamin" style="display: none;" class="col-md-7">
                                <select>
                                    <?php $items = array("L" => "Laki-Laki", "P" => "Perempuan"); ?>
                                    @foreach ($items as $key => $item)
                                        <option value="{{ $key }}" {{ ( $key == $user['jenis_kelamin'] ) ? 'selected' : '' }}> {{ $item }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <br><br>
                        <h3>{{__('all.ubahpassword')}}</h3>
                       
                        <div class="row" style="padding:5px;">
                            <div class="col-md-3">{{__('all.newpassword')}}</div><div class="col-md-1">:</div>
                            <div id ="txt-no-hp" class="col-md-7">
                                <input type="password" name="new_password" id="new_password" class="form-control">
                            </div>
                        </div>
                        <div class="row" style="padding:5px;">
                            <div class="col-md-3">{{__('all.konfirmasipass')}}</div><div class="col-md-1">:</div>
                            <div id ="txt-no-hp" class="col-md-7">
                                <input type="password" name="confirm_password" id="confirm_password" class="form-control">
                            </div>
                        </div>
                        <div class="row" style="padding:5px;">
                            <div class="col-md-3"></div><div class="col-md-1"></div>
                            <div id="btn-ubh" class="col-md-7"><button type="button" data="form_ubah" id="btn_ubah" class="site-btn btn-outline-primary btn-sm">Ubah</button></div>
                            <div id="btn-update" style="display: none;" class="col-md-7">
                                <button type="button" data="form_ubah" id="btn-updates" class="site-btn btn-outline-success btn-sm">Update</button>
                                <button type="button" data="form_ubah" id="btn_cancel" class="site-btn btn-outline-danger btn-sm">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
                <br/><br/>
            </div>
        </div>
    </div>
</div>

@section('js')
    <script type="text/javascript">
        $(document).ready(function(){
            
        });
        btn_cancel
        $("#btn_cancel").click(function(){
            $('#txt-nama').show();
            $('#input-nama').hide();
            $('#txt-email').show();
            $('#input-email').hide();
            $('#txt-alamat').show();
            $('#input-alamat').hide();
            $('#txt-jenis-kelamin').show();
            $('#input-jenis-kelamin').hide();
            $('#txt-tgl-lhr').show();
            $('#input-tgl-lhr').hide();
            $('#txt-no-hp').show();
            $('#input-no-hp').hide();
            $("#btn_ubah").show();
            $('#btn-update').hide();
        });
        $("#btn_ubah").click(function(){
            $('#txt-nama').hide();
            $('#input-nama').show();
            $('#txt-email').hide();
            $('#input-email').show();
            $('#txt-alamat').hide();
            $('#input-alamat').show();
            $('#txt-jenis-kelamin').hide();
            $('#input-jenis-kelamin').show();
            $('#txt-tgl-lhr').hide();
            $('#input-tgl-lhr').show();
            $('#txt-no-hp').hide();
            $('#input-no-hp').show();
            $(this).hide();
            $('#btn-update').show();
        });

        var password = document.getElementById("new_password")
  ,     confirm_password = document.getElementById("confirm_password");

        $(document).on("click", "#btn-updates", function(e){
            e.stopPropagation();
            if($('#new_password').val() != $('#confirm_password').val()) {
                Swal.fire({
                    title: 'Error!',
                    text: "Password dan Konfirmasi harus sama",
                    type: "error",
                    confirmButtonText: 'Ulangi'
                });
               
                return false;
               
                length > 0 
            }
            if($('#new_password').val().length < 4 ) {
                Swal.fire({
                    title: 'Error!',
                    text: "Password minimal harus 5 karakter",
                    type: "error",
                    confirmButtonText: 'Ulangi'
                });
               
                return false;
               
            }

            var id = $(this).attr('data');
            var ax = $("#"+id)[0];
            var dat = new FormData(ax);
            /*if($("#"+id).valid()==true){*/
            $.ajax({
                dataType:'json',
                type: 'POST',
                url: '{{route('update_profile')}}',
                data: dat, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                processData: false,
                contentType: false,
                enctype: 'multipart/form-data',
                success: function(data){
                console.log(data);
                if(data.status=='success'){
                    Swal.fire({
                        title: 'Success!',
                        text: data.message,
                        type: data.status,
                        confirmButtonText: 'OK'
                    });
                    // setInterval(function () {
                    //     window.location.replace("{{route('profile')}}");
                    // }, 23*100);
                   
                }else{
                    Swal.fire({
                        title: 'Error!',
                        text: data.message,
                        type: data.status,
                        confirmButtonText: 'Ulangi'
                    });
                }
                }
            });
        });

        $("#v-pills-home-tab").click(function() {
            $("#v-pills-home-tab").attr('class','nav-link active');
            $("#v-pills-profile-tab").attr('class','nav-link');
            $("#v-pills-history").attr('class','tab-pane fade show active in');
            $("#v-pills-profile").attr('class','tab-pane fade show');
        });
        $("#v-pills-profile-tab").click(function() {
            $("#v-pills-home-tab").attr('class','nav-link ');
            $("#v-pills-profile-tab").attr('class','nav-link active');
            $("#v-pills-history").attr('class','tab-pane fade show ');
            $("#v-pills-profile").attr('class','tab-pane fade show active in');
        });
    </script>
@endsection