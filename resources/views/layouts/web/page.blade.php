<div class="home-page-limestone">
    <div class="container">
        <div class="row align-items-end">
            <div class="coL-12 col-lg-6">
                <div class="section-heading">
                    <h4 class="entry-title">{{__('all.slog')}}</h4>
                </div><!-- .section-heading -->
            </div><!-- .col -->

            <div class="col-12 col-lg-6">
                <div class="milestones d-flex flex-wrap justify-content-between">
                    <div class="col-12 col-sm-6 mt-5 mt-lg-0">
                        <div class="counter-box">
                            <div class="d-flex justify-content-center align-items-center">
                                <img src="{{ asset('template.web/css/images/teamwork.png') }}" alt="">
                            </div>

                            <div class="d-flex justify-content-center align-items-baseline">
                                <div class="start-counter" data-to="{{$totalorangbaik}}" data-speed="2000"></div>
                            </div>

                            <h3 class="entry-title">#{{__('all.donor')}}</h3>

                        </div><!-- counter-box -->
                    </div><!-- .col -->

                    <div class="col-12 col-sm-6 mt-5 mt-lg-0">
                        <div class="counter-box">
                            <div class="d-flex justify-content-center align-items-center">
                                <img src="{{ asset('template.web/css/images/donation.png') }}" alt="">
                            </div>

                            <div class="d-flex justify-content-center align-items-baseline">
                                <div class="start-counter" data-to="{{$totalcampaign}}" data-speed="2000"></div>
                            </div>

                            <h3 class="entry-title">#TotalCampaign</h3>
                        </div><!-- counter-box -->
                    </div><!-- .col -->

                </div>
            </div><!-- .col -->
        </div><!-- .row -->
    </div><!-- .container -->
</div><!-- .our-causes -->