<div class="welcome-wrap">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-6 order-2 order-lg-1">
                <div class="welcome-content">
                    <header class="entry-header">
                        <h2 class="entry-title" style="color:#252525">{{__('all.headabout')}}</h2>
                    </header><!-- .entry-header -->

                    <div class="entry-content mt-5" style="color:#100f0f">
                        <p>{{__('all.deschead')}}</p>
                    </div><!-- .entry-content -->

                    <div class="entry-footer mt-5">
                        &nbsp;
                    </div><!-- .entry-footer -->
                </div><!-- .welcome-content -->
            </div><!-- .col -->

            <div class="col-12 col-lg-6 order-1 order-lg-2">
                <img src="{{ asset('template.web/css/images/donasi.jpeg') }}" alt="Donasi Palestina" style="width:100%">
            </div><!-- .col -->
        </div><!-- .row -->
    </div><!-- .container -->
</div><!-- .home-page-icon-boxes -->



<div class="about-testimonial">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-5">
                <div class="testimonial-cont">
                    <div class="entry-content">
                        <p>{{__('all.wow1')}}</p>
                    </div>

                </div>
            </div>

            <div class="col-12 col-md-6 offset-lg-2 col-lg-5">
                <div class="testimonial-cont">
                    <div class="entry-content">
                        <p>{{__('all.wow2')}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>