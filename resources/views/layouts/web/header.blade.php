<header class="site-header">
    <div class="top-header-bar">
        <div class="container">
            <div class="row flex-wrap justify-content-center justify-content-lg-between align-items-lg-center">
                <div class="col-12 col-lg-7 d-none d-md-flex flex-wrap justify-content-center justify-content-lg-start mb-3 mb-lg-0">
                    <div class="header-bar-email">
                        Email: <a href="mailto:info@muslimapp.id">info@muslimapp.id</a>
                    </div><!-- .header-bar-email -->

                    <div class="header-bar-text">
                        Contact: <a href="https://api.whatsapp.com/send?phone=628112333724" style="color:#defaed">+62 811 2333 724</a>
                    </div><!-- .header-bar-text -->

                </div><!-- .col -->

                <div class="col-12 col-lg-5 d-flex flex-wrap justify-content-center justify-content-lg-end align-items-center">
                    <div class="donate-btn">
                        @if (Session::get('user'))
                        <?php $ses = Session::get('user'); ?>
                            {{__('all.sloganhead')}} {{ $ses['nama'] }} 
                            <a href="{{ route('log_out') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" 
                            style="padding:2px 10px">
                                <i class="fa fa-power-off m-r-5 m-l-5"></i>
                            </a>
                            <form id="logout-form" action="{{ route('log_out') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        @else 
                            <a href="{{route('login')}}">{{__('all.Sign')}}</a>
                        @endif
                    </div><!-- .donate-btn -->
                </div><!-- .col -->
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- .top-header-bar -->

    <div class="nav-bar">
        <div class="container">
            <div class="row">
                <div class="col-12 d-flex flex-wrap justify-content-between align-items-center">
                    <div class="site-branding d-flex align-items-center">
                        <a class="d-block" href="{{url('/')}}" rel="home">
                            <img class="d-block" src="{{ asset('template.web/css/images/logo.png') }}" alt="logo">
                        </a>
                    </div><!-- .site-branding -->

                    <nav class="site-navigation d-flex justify-content-end align-items-center">
                        <ul class="d-flex flex-column flex-lg-row justify-content-lg-end align-content-center">
                            <li <?=(Request::is('/')) ? 'class="current-menu-item"': ''?>><a href="{{ url('/') }}">Home</a></li>
                            <li <?=(Route::is('donasi')) ? 'class="current-menu-item"': ''?>><a href="{{ route('donasi', [ 'detail' => 1 ]) }}">{{__('all.HeadDonate')}}</a></li>
                            <li <?=(Route::is('about')) ? 'class="current-menu-item"': ''?>><a href="{{ route('about') }}">{{__('all.AboutUs')}}</a></li>
                            <li><a href="https://news.muslimapp.id" target="_blank">{{__('all.News')}}</a></li>
                            <!-- <li><a href="#">Zakat</a></li> -->
                            <!-- <li><a href="#">Kontak</a></li> -->
                            <?php 
                                if (Session::get('user')) {
                                    $user = Session::get('user');
                                    if ($user['campaign_id']) { ?>
                                         <li <?=(Route::is('profile')) ? 'class="current-menu-item"': ''?>> <?php
                                            echo '<a href="'.route('profile').'" style="color:chocolate">';
                                                    echo 'Profile';
                                            echo '</a>';
                                        echo '</li>'; ?>
                                        <li> <?php
                                            echo '<a href="https://payments.muslimapp.id" style="color:chocolate" target="_blank">';
                                                    echo __('all.Confirmpayment');
                                            echo '</a>';
                                        echo '</li>';
                                    }
                                } else {
                                    $user = '';
                                }
                            ?>
                        </ul>
                    </nav><!-- .site-navigation -->

                    <div class="hamburger-menu d-lg-none">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div><!-- .hamburger-menu -->
                </div><!-- .col -->
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- .nav-bar -->
</header><!-- .site-header -->