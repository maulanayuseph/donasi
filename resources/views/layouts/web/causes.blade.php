<div class="our-causes">
    <div class="container">
        <div class="row">
            <div class="coL-12">
                <div class="section-heading">
                    <h2 id="section_bantuan" class="entry-title">{{__('all.readydonate')}}</h2>
                    <h6>{{__('all.choosedonate')}}</h6>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="swiper-container causes-slider">
                    <div class="swiper-wrapper">
                        <?php $no = 1; ?>
                        @foreach($data as $get)
                        <div class="swiper-slide">
                            <div class="cause-wrap">
                                <figure class="m-0">
                                    <img src="{{$get['campaign_image']}}" alt="">

                                    <div class="figure-overlay d-flex justify-content-center align-items-center position-absolute w-100 h-100">
                                        <a href="{{ route('donasi', [ 'detail' => $get['campaign_id'] ]) }}" class="btn gradient-bg mr-2">Donasi Sekarang</a>
                                    </div>
                                </figure>

                                <div class="cause-content-wrap">
                                    <header class="entry-header d-flex flex-wrap align-items-center">
                                        <h3 class="entry-title w-100 m-0" >
                                            <a href="{{ route('donasi', [ 'detail' => $get['campaign_id'] ]) }}">{{str_limit($get['campaign_title'],50)}}</a>
                                        </h3>
                                    </header>

                                    <div class="entry-content">
                                        <p class="m-0">
                                            <b>{{$get['city'][0]['name']}}</b>
                                        </p>
                                        <p class="m-0">
                                            <?php 
                                                $awal  = date_create();
                                                $akhir = date_create($get['campaign_valid']);
                                                $sisa  = date_diff( $awal, $akhir );
                                            ?>
                                           <b>{{$sisa->days}} {{__('all.daysagain')}} | {{$jumlah_donatur[$get['campaign_id']]}} {{__('all.donor')}}</b>
                                        </p>
                                    </div>

                                    <div class="fund-raised w-100">
                                        <div class="fund-raised-bar-{{$no}} barfiller">
                                            <div class="tipWrap">
                                                <span class="tip"></span>
                                            </div>
                                            <?php 
                                                $percen = ($get['campaign_total_balance']/$get['campaign_target'])*100; 
                                                $rond   = round($percen);
                                           ?>
                                            <span class="fill" data-percentage="{{$rond}}"></span>
                                        </div>

                                        <div class="fund-raised-details d-flex flex-wrap justify-content-between align-items-center">
                                            <div class="fund-raised-total mt-4">
                                                <b>{{__('all.terkumpul')}}: Rp. {{number_format($get['campaign_total_balance'])}}</b>
                                            </div><br/>

                                            <div class="fund-raised-goal mt-4">
                                                {{__('all.target')}} : Rp. {{number_format($get['campaign_target'])}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php $no++ ?>
                        @endforeach
                        
                    </div><!-- .swiper-wrapper -->

                </div><!-- .swiper-container -->

                <!-- Add Arrows -->
                <div class="swiper-button-next flex justify-content-center align-items-center">
                    <span><svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1171 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"/></svg></span>
                </div>

                <div class="swiper-button-prev flex justify-content-center align-items-center">
                    <span><svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1203 544q0 13-10 23l-393 393 393 393q10 10 10 23t-10 23l-50 50q-10 10-23 10t-23-10l-466-466q-10-10-10-23t10-23l466-466q10-10 23-10t23 10l50 50q10 10 10 23z"/></svg></span>
                </div>
            </div><!-- .col -->
        </div><!-- .row -->
    </div><!-- .container -->
</div><!-- .our-causes -->

<?php $json = json_encode($data); ?>

@section('js')
    <script type="text/javascript">

    </script>
@endsection