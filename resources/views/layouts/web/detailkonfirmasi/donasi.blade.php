<div class="page-area cart-page spad">
    <div class="container">
        <form class="confirmation-form" method="POST" action="{{route('saveconfirmation')}}" id="confirmation-form" enctype="multipart/form-data">
        {{ csrf_field() }}
            <div class="row">
                <div class="col-lg-6">
                    <h4 class="checkout-title"></h4>
                    @if($errors->any())
                        <div class="alert alert-danger" role="alert">
                            {{$errors->first()}}
                        </div>
                    @endif
                    <br/>

                    <div class="alert alert-success" role="alert">
                        <h4 class="alert-heading">{{__('all.alertconfirm')}}</h4>
                        <p>
                            <h6>OrderID# <b>{{$order_id}}</b></h6>
                            <h6>{{$get_order['nama_pemesan']}}</h6>
                            <h6>{{$get_order['alamat']}}</h6>
                            <h6>{{$get_order['email']}}</h6>
                            <h6>{{$get_order['no_hp']}}</h6>
                            <h6>{{$get_order['catatan']}}</h6>
                            <h6>{{$get_order['kuisioner']}}</h6>
                        </p>
                        <hr>
                        <p class="mb-0">
                            @if(!empty(Session::get('result_va')))
                                <?php $sesva = Session::get('result_va'); ?>
                                <h6><b>{{$sesva['status_message']}}</b></h6>
                                <h6>Biller Code&nbsp; : Midtrans ({{$sesva['biller_code']}})</h6>
                                <h6>Number VA&nbsp;: {{$sesva['bill_key']}}</h6>
                                <h6>Total {{__('all.Nominal')}} : Rp. {{number_format($sesva['gross_amount'])}}</h6>
                            @else
                                <h6>{{$get_bank['nama_bank']}}</h6>
                                <h6>{{$get_bank['no_rek_bank']}}</h6>
                                <h6>{{$get_bank['atas_nama']}}</h6>
                                <h6>Total {{__('all.Nominal')}} : Rp. {{number_format($get_order['nominal'])}}</h6>
                            @endif
                        </p>
                        <hr>
                        @if(!empty(Session::get('result_va')))
                        <p class="mb-0" style="font-size:0.8rem">
                            Lakukan Pembayaran Maks 1x24 Jam Jika Pembayaran Belum Dilakukan Maka Kami Anggap
                            Dibatalkan dan Lakukan Order Donasi Kembali. <br/>
                            <b><a href="#" data-toggle="modal" data-target="#detail_modal" style="color:#162960">
                                Panduan Pembayaran Virtual Account Mandiri
                            </a></b>
                        </p>
                        @else
                        <p class="mb-0" style="font-size:0.8rem">
                            Lakukan Pembayaran Maks 1x24 Jam dan Lakukan Konfirmasi Pembayaran dengan Klik Confirm Payment dengan
                            Memasukkan Order ID, Jika Pembayaran Belum Dilakukan Maka Kami Anggap Dibatalkan dan Lakukan Order Donasi Kembali. <br/>
                        </p>
                        @endif
                    </div>

                    <br/>
                    @if(empty(Session::get('result_va')))
                    <div class="row">
                        <div class="col-md-12">
                            <a href="https://payments.muslimapp.id" target="_blank" id="submit" class="btn btn-full btn-primary" style="border:0px;color:#fff">
                                <span id="loading">Confirm Payment</span>
                            </a>
                        </div>
                    </div>
                    @endif
                </div>

                <div class="modal fade" id="detail_modal">
                    <div class="modal-dialog modal-lg" style="">
                    <div class="modal-content">
                    
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Panduan Pembayaran Virtual Account Mandiri</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link" type="button" onclick="collapse_trf(1)"  style="padding:0px;border:0px;font-size:1rem;color:#355725;line-height:2">
                                                Melalui transfer ATM
                                            </button>
                                        </h2>
                                    </div>
                                    
                                    <div id="collapsetrf1" data-v='0' style="display:none">
                                        <div class="border">
                                            <div class="row">
                                                <div class="col-md-12 mb-4" style="text-align:left;font-size:0.8rem">
                                                    1. Silahkan pilih Bayar/Beli <br/>
                                                    2. Kemudian pilih Lainnya > Lainnya > Multi Payment <br/>
                                                    3. Silahkan masukkan kode perusahaan 70012 dan pilih Benar <br/>
                                                    4. Masukkan kode bayar dengan nomor Virtual Account Anda (contoh: 7001202001539202) dan klik Benar <br/>
                                                    5. Jangan lupa untuk memeriksa informasi yang tertera pada layar. Pastikan Merchant adalah DomaiNesia dan total tagihan sudah benar. Jika benar, tekan angka 1 dan pilih Ya
                                                    6. Periksa layar konfirmasi dan pilih Ya
                                                </div>
                                            </div>
                                        </div>
                                    </div>    

                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link" type="button" onclick="collapse_trf(2)"  style="padding:0px;border:0px;font-size:1rem;color:#355725;line-height:2">
                                                Melalui Transfer mBanking
                                            </button>
                                        </h2>
                                    </div>
                                    
                                    <div id="collapsetrf2" data-v='0' style="display:none">
                                        <div class="border">
                                            <div class="row">
                                                <div class="col-md-12 mb-4" style="text-align:left;font-size:0.8rem">
                                                    1. Silahkan login ke mobile banking Anda.<br/>
                                                    2. Klik <b>"Icon Menu"</b> di sebelah kiri atas. <br/>
                                                    3. Pilih menu <b>"Pembayaran"</b> <br/>
                                                    4. Pilih buat <b>"Pembayaran Baru"</b> <br/>
                                                    5. Pilih <b>"Multi Payment"</b> <br/>
                                                    6. Klik <b>"Penyedia Jasa"</b> atau <b>"Service Provider"</b>,  kemudian pilih kode perusahaan, contoh:  <b>"Midtrans 70012"</b> <br/>
                                                    7. Pilih <b>"No. Virtual"</b> <br/>
                                                    8. Masukkan nomor Virtual Account Anda dengan kode perusahaan (contoh 7001202001287578), kemudian pilih <b>"Tambah &nbsp;&nbsp;&nbsp;&nbsp;Sebagai Nomor Baru"</b> <br/>
                                                    9. Masukkan nominal lalu pilih <b>"Konfirmasi"</b> dan <b>"lanjut"</b> <br/>
                                                    10. Selanjutnya akan muncul tampilan konfirmasi pembayaran. Pastikan semua informasi dan total tagihan sudah benar. Jika &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sudah benar, lalu scroll ke bawah  dan pilih "Konfirmasi" <br/>
                                                    11. <b>Masukkan PIN Anda</b> dan pilih <b>"OK"</b> <br/>
                                                    12. Sampai pada tahap ini, berarti transaksi dengan menggunakan VA telah berhasil dilakukan.
                                                </div>
                                            </div>
                                        </div>
                                    </div>  

                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link" type="button" onclick="collapse_trf(3)"  style="padding:0px;border:0px;font-size:1rem;color:#355725;line-height:2">
                                                Melalui Internet Banking
                                            </button>
                                        </h2>
                                    </div>
                                    
                                    <div id="collapsetrf3" data-v='0' style="display:none">
                                        <div class="border">
                                            <div class="row">
                                                <div class="col-md-12 mb-4" style="text-align:left;font-size:0.8rem">
                                                    1. Login ke website Mandiri Online dengan memasukkan user ID dan PIN <br/>
                                                    2. Pilih menu <b>"Pembayaran"</b> <br/>
                                                    3. Pilih menu <b>"Multi Payment"</b> <br/>
                                                    4. Silahkan pilih <b>"No Rekening Anda"</b> <br/>
                                                    5. Pilih <b>"Penyedia Jasa"</b>, contoh: <b>"Midtrans 70012"</b> <br/>
                                                    6. Pilih <b>"No Virtual Account"</b> <br/>
                                                    7. Masukkan <b>"Nomor Virtual Account"</b> <br/>
                                                    8. Masuk ke halaman konfirmasi 1 <br/>
                                                    9. Apabila sudah sesuai, klik <b>"Tagihan Total"</b>, kemudian <b>"Lanjutkan"</b> <br/>
                                                    10. Masuk ke halaman konfirmasi 2 <br/>
                                                    11. Masukkan Challenge Code yang dikirimkan ke Token Internet Banking Anda, kemudian <b>"Kirim"</b>
                                                    Anda akan masuk ke <br/>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;halaman konfirmasi jika pembayaran telah selesai
                                                </div>
                                            </div>
                                        </div>
                                    </div>  

                                </div>
                            </div>
                        </div>
                        
                    </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="order-card">
                        <div class="order-details">
                            <div class="od-warp">
                                <h4 class="checkout-title">{{__('all.nodonasi')}} #{{$data['campaign_id']}}</h4>
                                <input type="hidden" value="{{$data['campaign_id']}}" name="campaign_id">
                                <div class="cause-wrap d-flex flex-wrap justify-content-between">
                        <figure class="m-0">
                            <img src="{{$data['campaign_image']}}" alt="">
                        </figure>

                        <div class="cause-content-wrap">
                            <header class="entry-header d-flex flex-wrap align-items-center">
                                <h3 class="entry-title w-100 m-0">
                                    <a href="#">{{$data['campaign_title']}}</a>
                                </h3>

                                <div class="posted-date">
                                    <a href="javascript:;">
                                        <?php $date = date_create($data['campaign_created']); echo date_format($date,"Y/m/d H:i:s");?>
                                    </a>
                                </div><!-- .posted-date -->

                                <div class="posted-date">
                                    <?php 
                                        $hariini    = new DateTime();
                                        $terakhir   = new DateTime($data['campaign_valid']);
                                        $sisa       = $terakhir->diff($hariini)->format("%a");
                                    ?>
                                    <a href="javascript:;">
                                        {{$sisa}} {{__('all.daysagain')}}
                                    </a>
                                </div>

                                <div class="cats-links">
                                    <a href="javascript:;">{{$data['city'][0]['name']}}</a>
                                </div><!-- .cats-links -->
                            </header><!-- .entry-header -->

                            <div class="entry-content" style="text-align:justify">
                                <p class="m-0">{{$data['campaign_description']}}</p>
                            </div><!-- .entry-content -->
                        </div>
                        
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

@section('js_checkout')
    <script type="text/javascript">
    </script>
@endsection