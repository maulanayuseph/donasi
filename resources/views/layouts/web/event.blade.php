<div class="home-page-events">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="upcoming-events">
                    <div class="section-heading">
                        <h2 class="entry-title">Rangkaian Acara</h2>
                    </div><!-- .section-heading -->

                    <div class="event-wrap d-flex flex-wrap justify-content-between">
                        <figure class="m-0">
                            <img src="{{ asset('template.web/css/images/event-1.jpg') }}" alt="">
                        </figure>

                        <div class="event-content-wrap">
                            <header class="entry-header d-flex flex-wrap align-items-center">
                                <h3 class="entry-title w-100 m-0"><a href="#">Bantu Korban Tsunami Selat Sunda</a></h3>

                                <div class="posted-date">
                                    <a href="#">Feb 25, 2019 </a>
                                </div><!-- .posted-date -->

                                <div class="cats-links">
                                    <a href="#">Badan Wakaf Salman</a>
                                </div><!-- .cats-links -->
                            </header><!-- .entry-header -->

                            <div class="entry-content">
                                <p class="m-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris tempus vestib ulum mauris.</p>
                            </div><!-- .entry-content -->

                            <div class="entry-footer">
                                <a href="#">Selengkapnya</a>
                            </div><!-- .entry-footer -->
                        </div><!-- .event-content-wrap -->
                    </div><!-- .event-wrap -->

                    <div class="event-wrap d-flex flex-wrap justify-content-between">
                        <figure class="m-0">
                            <img src="{{ asset('template.web/css/images/event-2.jpg') }}" alt="">
                        </figure>

                        <div class="event-content-wrap">
                            <header class="entry-header d-flex flex-wrap align-items-center">
                                <h3 class="entry-title w-100 m-0"><a href="#">Air Bersih untuk Cikapundung</a></h3>

                                <div class="posted-date">
                                    <a href="#">Aug 25, 2018 </a>
                                </div><!-- .posted-date -->

                                <div class="cats-links">
                                    <a href="#">IBCC Bandung</a>
                                </div><!-- .cats-links -->
                            </header><!-- .entry-header -->

                            <div class="entry-content">
                                <p class="m-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris tempus vestib ulum mauris.</p>
                            </div><!-- .entry-content -->

                            <div class="entry-footer">
                                <a href="#">Selengkapnya</a>
                            </div><!-- .entry-footer -->
                        </div><!-- .event-content-wrap -->
                    </div><!-- .event-wrap -->

                    <div class="event-wrap d-flex flex-wrap justify-content-between">
                        <figure class="m-0">
                            <img src="{{ asset('template.web/css/images/event-3.jpg') }}" alt="">
                        </figure>

                        <div class="event-content-wrap">
                            <header class="entry-header d-flex flex-wrap align-items-center">
                                <h3 class="entry-title w-100 m-0"><a href="#">Bantu Anak Yatim di Sumba</a></h3>

                                <div class="posted-date">
                                    <a href="#">Aug 25, 2018 </a>
                                </div><!-- .posted-date -->

                                <div class="cats-links">
                                    <a href="#">Sumba Kalimantan Timur</a>
                                </div><!-- .cats-links -->
                            </header><!-- .entry-header -->

                            <div class="entry-content">
                                <p class="m-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris tempus vestib ulum mauris.</p>
                            </div><!-- .entry-content -->

                            <div class="entry-footer">
                                <a href="#">Read More</a>
                            </div><!-- .entry-footer -->
                        </div><!-- .event-content-wrap -->
                    </div><!-- .event-wrap -->
                </div><!-- .upcoming-events -->
            </div><!-- .col -->

            <div class="col-12 col-lg-6">
                <div class="featured-cause">
                    <div class="section-heading">
                        <h2 class="entry-title">Bantuan Cepat!</h2>
                    </div><!-- .section-heading -->

                    <div class="cause-wrap d-flex flex-wrap justify-content-between">
                        <figure class="m-0">
                            <img src="{{ asset('template.web/css/images/featured-causes.jpg') }}" alt="">
                        </figure>

                        <div class="cause-content-wrap">
                            <header class="entry-header d-flex flex-wrap align-items-center">
                                <h3 class="entry-title w-100 m-0"><a href="#">Bencana Tsunami Selat Sunda</a></h3>

                                <div class="posted-date">
                                    <a href="#">Apr 25, 2019 </a>
                                </div><!-- .posted-date -->

                                <div class="cats-links">
                                    <a href="#">Banten Jawa Barat</a>
                                </div><!-- .cats-links -->
                            </header><!-- .entry-header -->

                            <div class="entry-content">
                                <p class="m-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris tempus vestib ulum mauris. Lorem ipsum dolor sit amet, consectetur.</p>
                            </div><!-- .entry-content -->

                            <div class="entry-footer mt-5">
                                <a href="donasi.html" class="btn gradient-bg mr-2">Donasi Cepat</a>
                            </div><!-- .entry-footer -->
                        </div><!-- .cause-content-wrap -->

                        <div class="fund-raised w-100">
                            <div class="featured-fund-raised-bar barfiller">
                                <div class="tipWrap">
                                    <span class="tip"></span>
                                </div><!-- .tipWrap -->

                                <span class="fill" data-percentage="83"></span>
                            </div><!-- .fund-raised-bar -->

                            <div class="fund-raised-details d-flex flex-wrap justify-content-between align-items-center">
                                <div class="fund-raised-total mt-4">
                                    <a class="btn gradient-bg">Terkumpul: Rp.20.653.880</a>
                                </div><!-- .fund-raised-total -->

                                <div class="fund-raised-goal mt-4">
                                    Target: Rp. 100.000.000
                                </div><!-- .fund-raised-goal -->
                            </div><!-- .fund-raised-details -->
                        </div><!-- .fund-raised -->
                    </div><!-- .cause-wrap -->
                </div><!-- .featured-cause -->
            </div><!-- .col -->
        </div><!-- .row -->
    </div><!-- .container -->
</div><!-- .home-page-events -->