<div class="page-area cart-page spad">
    <div class="container">
        <form class="confirmation-form" method="POST" action="{{route('saveconfirmation')}}" id="confirmation-form" >
            {{ csrf_field() }}
            <div class="row">
                <div class="col-lg-6">
                    <h4 class="checkout-title">{{__('all.formdonasi')}}</h4>
                    @if($errors->any())
                        <div class="alert alert-danger" role="alert">
                            {{$errors->first()}}
                        </div>
                    @endif
                    <br/>
                    <div class="alert alert-success" role="alert">
                        <h4 class="alert-heading">Biodata</h4>
                        <p>
                            <h6>{{$user['nama']}}</h6>
                            <h6>{{$user['alamat']}}</h6>
                            <h6>{{$user['email']}}</h6>
                            <h6>{{$get_order['no_hp']}}</h6>
                            <h6>{{$get_order['catatan']}}</h6>
                            <h6>{{$get_order['kuisioner']}}</h6>
                        </p>
                        <hr>
                        <p class="mb-0">
                            <h6>Total {{__('all.Nominal')}} : Rp. {{number_format($get_order['nominal'])}}</h6>
                        </p>
                    </div>
                    <br/>
                    <h4><b>{{__('all.MetodePembayaran')}}</b></h4>
                    <div class="row">
                        <div class="col-md-12">
                        <?php $num=0; ?>

                            @foreach($allbank['bank'] as $typebank => $id)
                                @if($typebank == 'TF')
                                <h6 style="padding:10px"><b>Transfer Bank (Verifikasi Manual)</b></h6>
                                    @foreach($getBank as $bank)
                                        @foreach($id['id'] as $idbank)
                                            @if($idbank == $bank['id'])
                                            <div class="card-header" id="headingOne">
                                                <h2 class="mb-0">
                                                    <button class="btn btn-link" type="button" onclick="collapse({!!$bank['id']!!})" style="padding:0px;border:0px;font-size:1rem;color:#355725;line-height:2">
                                                        {{$bank['nama_bank']}}
                                                    </button>
                                                </h2>
                                            </div>
                                            
                                            <div id="collapselistbank{!!$bank['id']!!}" data-v='0' style="display:none">
                                                <div class="border">
                                                    <div class="row">
                                                        <div class="col-md-2 mb-4" style="text-align:center;margin-top:40px">
                                                            <input type="radio" class="checkmark" id="bank{!!$bank['id']!!}" value="{{$bank['id']}}" name="bank_id">
                                                        </div>
                                                        <div class="col-md-4 mb-4" style="text-align:center;margin-top:25px">
                                                            <img src="{{$bank['logo']}}" class="align-self-center col-md-12" alt="{{$bank['nama_bank']}}" >
                                                            <br/>
                                                        </div>
                                                        <div class="col-md-6 mb-4" style="text-align:center;margin-top:45px">
                                                        {{$bank['no_rek_bank']}} <br/> A/n {{$bank['atas_nama']}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>        
                                            @endif
                                        @endforeach
                                    @endforeach
                                @elseif($typebank == 'VA')
                                    <h6 style="padding:10px"><b>Transfer Virtual Account (Verifikasi Otomatis)</b></h6>
                                    @foreach($getBank as $bank)
                                        @foreach($id['id'] as $idbank)
                                            @if($idbank == $bank['id'])
                                            <div class="card-header" id="headingOne">
                                                <h2 class="mb-0">
                                                    <button class="btn btn-link" type="button" onclick="collapse({!!$bank['id']!!})" style="padding:0px;border:0px;font-size:1rem;color:#355725;line-height:2">
                                                        {{$bank['nama_bank']}}
                                                    </button>
                                                </h2>
                                            </div>
                                            
                                            <div id="collapselistbank{!!$bank['id']!!}" data-v='0' style="display:none">
                                                <div class="border">
                                                    <div class="row">
                                                        <div class="col-md-2 mb-4" style="text-align:center;margin-top:40px">
                                                            <input type="radio" required class="checkmark" id="bank{!!$bank['id']!!}" value="{{$bank['id']}}|{{$bank['type']}}" name="bank_id">
                                                        </div>
                                                        <div class="col-md-4 mb-4" style="text-align:center;margin-top:25px">
                                                            <img src="{{$bank['logo']}}" class="align-self-center col-md-12" alt="{{$bank['nama_bank']}}" >
                                                            <br/>
                                                        </div>
                                                        <div class="col-md-6 mb-4" style="text-align:center;margin-top:45px">
                                                        {{$bank['no_rek_bank']}} 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>        
                                            @endif
                                        @endforeach
                                    @endforeach
                                @endif
                        @endforeach

                        </div>
                    </div>

                    
                    <br/>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" id="submit2" class="btn btn-full btn-primary" style="border:0px;color:#fff">
                                <span id="loading">Continue</span>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="order-card">
                        <div class="order-details">
                            <div class="od-warp">
                                <h4 class="checkout-title">{{__('all.nodonasi')}} #{{$data['campaign_id']}}</h4>
                                <input type="hidden" value="{{$data['campaign_id']}}" name="campaign_id">
                                <div class="cause-wrap d-flex flex-wrap justify-content-between">
                        <figure class="m-0">
                            <img src="{{$data['campaign_image']}}" alt="">
                        </figure>

                        <div class="cause-content-wrap">
                            <header class="entry-header d-flex flex-wrap align-items-center">
                                <h3 class="entry-title w-100 m-0">
                                    <a href="#">{{$data['campaign_title']}}</a>
                                </h3>

                                <div class="posted-date">
                                    <a href="javascript:;">
                                        <?php $date = date_create($data['campaign_created']); echo date_format($date,"Y/m/d H:i:s");?>
                                    </a>
                                </div><!-- .posted-date -->

                                <div class="posted-date">
                                    <?php 
                                        $hariini    = new DateTime();
                                        $terakhir   = new DateTime($data['campaign_valid']);
                                        $sisa       = $terakhir->diff($hariini)->format("%a");
                                    ?>
                                    <a href="javascript:;">
                                        {{$sisa}} {{__('all.daysagain')}}
                                    </a>
                                </div>

                                <div class="cats-links">
                                    <a href="javascript:;">{{$data['city'][0]['name']}}</a>
                                </div><!-- .cats-links -->
                            </header><!-- .entry-header -->

                            <div class="entry-content" style="text-align:justify">
                                <p class="m-0">{{$data['campaign_description']}}</p>
                            </div><!-- .entry-content -->
                        </div>
                        
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

@section('js_checkout')
    <script type="text/javascript">
        $("#submit2").click(function() {
            var form = document.getElementById('confirmation-form');
            var isValidForm = form.checkValidity();
            
            if (isValidForm == true) {
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": true,
                    "positionClass": "toast-top-center",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
                toastr['success']('Loading ...');
                $("#submit2").attr('disabled','disabled');
                $("#loading").html('Loading ...');
            } else {
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": true,
                    "positionClass": "toast-top-center",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
                toastr["error"]('Mohon Untuk Pilih Terlebih Dahulu Metode Pembayarannya');
                $("#submit2").removeAttr('disabled');
                $("#loading").html('Continue');
                return false;
            }
        });
    </script>
@endsection