<?php
    return [
        
        //header
        'Title'         => 'Muslimapp | Donasi Untuk Negeri',
        'HeadDonate'    => 'Kirim Donasi',
        'AboutUs'       => 'Tentang Kami',
        'News'          => 'Berita',
        'Sign'          => 'Daftar / Masuk',
        'Confirmpayment'=> 'Konfirmasi Pembayaran',
        'sloganhead'    => 'Selamat Berbuat Kebaikan',

        //slides
        'slidesbanner'  => 'Ayo berdonasi utk Masjid Gaza Palestina dengan melalui Download',

        //iconboxes
        'title1'        => 'Berdonasi untuk sesama',
        'slog1'         => 'Satukan Keberagaman Donasi untuk Keberdayaan Sesama. ',
        'title2'        => 'Bersedekah di jalan Allah',
        'slog2'         => 'Apa saja yang ada di sisimu akan habis, dan apa yang ada di sisi Allah adalah kekal. QS. An-Nahl: 96 ',
        'title3'        => 'Donasi Aman & Terpercaya',
        'slog3'         => 'Kami adalah tempat yang tepat untuk berdonasi aman dan terpercaya.',

        //causes
        'readydonate'   => 'Siap memberikan donasi ?',
        'choosedonate'  => 'Pilih Campaign yang ingin anda bantu',
        'daysagain'     => 'Hari Lagi',
        'donor'         => 'Donatur',
        'terkumpul'     => 'Terkumpul',
        'target'        => 'Target',

        //page 
        'slog'          => "Donasi dari kamu menyembuhkan separuh luka hati dunia. Mari bersama terus bersedekah untuk sesama!",

        //footer
        'Donasi'        => 'Donasi',
        'Testimoni'     => 'Testimonial',
        'event'         => 'Rangkaian Acara',

        //tentangkami
        'titleabout'    => 'Tentang Kami & Donasi',
        'headabout'     => 'Donasi Masjid di Gaza Palestina',
        'deschead'      => 'Wujudkan pembangunan masjid "Syeikh Ajlin di Gaza Palestina, muslimapp.id berkolaborasi bersama mewujudkan pembanunan masjid di Gaza Palestina bersama Badan Wakaf Salman, Aman - Palestin dan Jabar Peduli Palestina untuk bersatu bersama menggalang donasi untuuk semua pihak yang ingin ikut berdonasi dan mewujudkan masjid di Gaza-Palestina.',
        'wow1'          => 'Kepedulian kepada palestina merupakan visi umat muslim untuk saling bantu membantu.',
        'wow2'          => 'Penderitaan palestina adalah penderitaan kita semua, semua wajib menanggung beban khusunya umat muslim di seluruh belahan dunia.',

        //formdonasi
        'formdonasi'    => 'Form Donasi',
        'hideme'        => 'Sembunyikan Nama Saya',
        'signin'        => 'Masuk',
        'lengkapidata'  => 'atau Lengkapi data dibawah ini',
        'darimanatau'   => 'Dari Mana Anda Tahu Donasi Ini',
        'nodonasi'      => 'No.Donasi',
        'placenamalengkap' => 'Nama Lengkap *',
        'placeaddress'      => 'Alamat *',
        'placejumlahdonasi' => 'Jumlah Donasi *',
        'errnominal'        => 'Nominal harus kelipatan Rp. 10,000',
        'placecatatan'     => 'Tulis catatan dukungan atau doa untuk penggalangan dana ini.',

        //konfirmasi
        'Nominal'       => 'Nominal',
        'MetodePembayaran' => 'Metode Pembayaran',

        //detailkonfirmasi
        'alertconfirm' => 'Terima Kasih Telah Berdonasi Silahkan Lakukan Pembayaran',

        //profile
        'mydonasi'     => 'Donasi Saya',
        'nomdonasi'    => 'Nominal Donasi',
        'tanggal'      => 'Tanggal',
        'blmkonfirmasi' => 'Order Masuk',
        'sdhkonfirmasi' => 'Sudah Konfirmasi',
        'sdhbayar'      => 'Sudah Bayar',
        'namalengkap' => 'Nama Lengkap ',
        'alamat'      => 'Alamat',
        'tanggallahir' => 'Tanggal Lahir',
        'jeniskelamin' => 'Jenis Kelamin',
        'ubahpassword' => 'Ubah Password',
        'newpassword'   => 'Password Baru',
        'konfirmasipass' => 'Konfirmasi Password Baru'
    ];
?>