<?php
    return [
        
        //header
        'Title'         => 'Muslimapp | Donate For Country',
        'HeadDonate'    => 'Send Donate',
        'AboutUs'       => 'About Us',
        'News'          => 'News',
        'Sign'          => 'Register / Sign In',
        'Confirmpayment'=> 'Confirm Payment',
        'sloganhead'    => 'Congrats for your Kindness',
        
        //slides
        'slidesbanner'  => 'Come donate to Gaza Palestine Mosque through Download',

        //iconboxes
        'title1'        => 'Donate to Others',
        'slog1'         => 'Unite the Diversity of Donations for the Empowerment of Others. ',
        'title2'        => 'Giving alms on the way of Allah',
        'slog2'         => 'Whatever you have will end, but what Allah has is lasting. QS. An-Nahl: 96 ',
        'title3'        => 'Donation is safe & reliable',
        'slog3'         => 'We are the right place to donate safely and reliably.',

        //causes
        'readydonate'   => 'Ready for give Donate ?',
        'choosedonate'  => 'Choose campaign wants you help',
        'daysagain'     => 'Days Left',
        'donor'         => 'Donor',
        'terkumpul'     => 'Collected',
        'target'        => 'Target',

        //page 
        'slog'          => "Your donation heals half of the world's hurt. Let's continue to give charity to others!",
        
        //footer
        'Donasi'        => 'Donate',
        'Testimoni'     => 'Testimoni',
        'event'         => 'Event',

        //tentangkami
        'titleabout'    => 'About Us & Donate',
        'headabout'     => 'Donate Mosque in Gaza Palestine',
        'deschead'      => 'Realizing the construction of the mosque "Sheikh Ajlin in Gaza Palestine, Muslimapp.id collaborated together to realize the construction of mosques in Gaza Palestine along with the Salman Waqf Agency, Safe - Palestine and West Java Caring for Palestine to unite together to raise donations for all parties who wish to donate and realize the mosque in Gaza-Palestine.',
        'wow1'          => 'Concern for Palestine is the vision of Muslims to help each other help.',
        'wow2'          => 'The suffering of the Palestinians is the suffering of all of us, all must bear the burden, especially Muslims in all parts of the world.',
        
        //formdonasi
        'formdonasi'    => 'Form Donate',
        'hideme'        => 'Hide My Name',
        'signin'        => 'Sign In',
        'lengkapidata'  => 'or complete the data below.',
        'darimanatau'   => 'Where do you know about this Donation',
        'nodonasi'      => 'No.Donate',
        'placenamalengkap' => 'Full Name *',
        'placeaddress'      => 'Address *',
        'placejumlahdonasi' => 'Amount of Donation *',
        'errnominal'        => 'Amount of Donation must multiple Rp. 10,000',
        'placecatatan'     => 'Write a support or prayer note for this fundraising.',

         //konfirmasi
         'Nominal'       => 'Amount',
         'MetodePembayaran' => 'Payment Method',

         //detailkonfirmasi
         'alertconfirm' => 'Thank You for your Donation please make a payment',

        //profile
        'mydonasi'     => 'My Donation',
        'nomdonasi'    => 'Amount of Donation',
        'tanggal'      => 'Date',
        'blmkonfirmasi' => 'In Order',
        'sdhkonfirmasi' => 'Confirmed',
        'sdhbayar'      => 'Already Paid',
        'namalengkap' => 'Full Name',
        'alamat'      => 'Address',
        'tanggallahir' => 'Date of Birth',
        'jeniskelamin' => 'Gender',
        'ubahpassword' => 'Change Password',
        'newpassword'   => 'New Password',
        'konfirmasipass' => 'Confirmed New Password'

    ];
?>